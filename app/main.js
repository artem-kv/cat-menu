function eqHeight(group) {

	var eqH = 0;

	group.each(function() {
		var tH = $(this).height();
		if (tH > eqH) eqH = tH;
	});

	group.height(eqH);
}

	



$(document).ready(function () {

	jQuery.fn.catMenu = function() { 

		var $t      = $(this),
			$button = $t.find('.js-cat-button'),
			$drop   = $t.find('.js-cat-drop'),
			$dropUl = $t.find('.js-cat-drop>ul'),
			flag    = true;

		$dropUl
			.addClass('lev-1').children('li').children('ul')
			.addClass('lev-2').children('li').children('ul')
			.addClass('lev-3');

		var $lev1 = $drop.find('.lev-1'),
			$lev2 = $drop.find('.lev-2'),
			$lev3 = $drop.find('.lev-3');
			$img  = $drop.children('.img');			

		$drop
			.show()
			.height($dropUl.height() + 30)
			.hide();

		$button.click(function() {
			$drop
				.slideToggle(200);
		});

	//mouseenter lev-1

		$('.lev-1>li>a').on('mouseenter', function () {

			eqHeight($('.block'))

			var $t     = $(this),
				$tLev2 = $t.siblings('.lev-2'),
				lev1H  = $('.lev-1').height(),
				lev2H  = $t.siblings('.lev-2').height();

			$('.lev-1>li>a, .lev-2>li>a').removeClass('active');
			$t.addClass('active');

			dropH = Math.max(lev1H, lev2H);


				if (flag) {
					$drop
						.animate({height: dropH + 32, width: 830},280);
				}
	
				flag = false;
	
			//setTimeout(function () {
				$drop
					.stop(true)
					.animate({height: dropH + 32},150)
					.animate({width: 830},50);
	
				$('.lev-2')
					.not($tLev2)
					.css({left: -86});
	
				$tLev2
					.stop(true)
					.animate({left: 222}, 200);

				$('.lev-3')
					.css({left: -86});
	
				$('.js-cat-drop>a')
					.animate({right: 'auto', width: '611px'}, 150)
					.siblings('.img')
					.css({right: 'auto'})
					.animate({left: '-308px'}, 250);
	

			//}, 400);

			return false;

		});

	//mouseenter lev-2

		$('.lev-2>li>a').on('mouseenter', function () {

			var $t     = $(this),
				$tLev3 = $t.siblings('.lev-3'),
				lev1H  = $('.lev-1').height(),
				lev2H  = $t.parent().parent().height(),
				lev3H  = $tLev3.height(),
				imgH   = $('.js-cat-drop>.img .wrap').height();

			$('.lev-2>li>a').removeClass('active');
			$t.addClass('active');

			dropH = Math.max(lev1H, lev2H);
			dropH = Math.max(dropH, lev3H, imgH);

			$drop
				.stop(true)
				.animate({height: dropH + 32},150)
				.animate({width: 1140},50);

			$('.lev-3')
				.not($tLev3)
				.css({left: -86});	

			$tLev3
				//.stop()
				.animate({left: 305}, 150);

			$('.js-cat-drop>a')
				.animate({right: 305}, 150);

			$('.js-cat-drop>.img')
				.css({left: 'auto'})
				//.stop(true)
				.animate({right: '-1px'}, 200)
				.find('img')
				.attr('src', function () {
					return $t.data('img');
				})
				.end()
				.find('a')
				.attr('href', $t.data('href'))
				.text($t.data('href'));
		});


	};
	$('.cat-menu').catMenu();

});