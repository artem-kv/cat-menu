var gulp        = require('gulp'),
    less        = require('gulp-less'),
    jade        = require('gulp-jade'),
    prettify    = require('gulp-prettify'),
    notify      = require('gulp-notify' ),
    watch       = require('gulp-watch'),
    browserSync = require('browser-sync').create();

///////////////////////////////////////////////////

gulp.task('serve', function() {

    browserSync.init({
        server: "app"
    });

    gulp.watch("*.less", ['less']);
    gulp.watch("*.jade", ['jade']);
    gulp.watch("app/main.js").on('change', browserSync.reload);
    gulp.watch("*.less").on('change', browserSync.reload);
});

gulp.task('less', function() {
    return gulp.src("*.less")
        .pipe(less().on('error', notify.onError(
        {
            message: "<%= error.message %>",
            title  : "Less Error!"
        })))
        .pipe(gulp.dest("app"))
        .pipe(browserSync.stream());
});

gulp.task('jade', function() {
    return gulp.src("*.jade")
        .pipe(jade().on('error', notify.onError(
        {
            message: "<%= error.message %>",
            title  : "Jade Error!"
        })))
        .pipe(prettify({indent_char: ' ', indent_size: 4}))
        .pipe(gulp.dest("app"))
        .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);
